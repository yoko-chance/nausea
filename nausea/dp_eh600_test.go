package nausea

import (
	"strconv"
	"testing"
)

func TestMakeBitamapLine(t *testing.T) {
	p := &DpEh600{}

	b, err := p.makeBitmapLine([8]byte{1, 1, 0, 1, 0, 1, 0, 0})
	if err != nil {
		t.Error(err)
	}

	i, _ := strconv.ParseInt("11010100", 2, 64)
	if b != i {
		t.Errorf("expect %b, actual %b", i, b)
	}
}

func TestSplitBitmapList(t *testing.T) {
	p := &DpEh600{}

	bls := [][8]byte{
		{1, 1, 0, 1, 0, 1, 0, 0},
		{0, 0, 1, 0, 1, 0, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1},
		{0, 0, 0, 0, 0, 0, 0, 0},
	}

	var l []byte
	l = append(l, bls[0][:]...)
	l = append(l, bls[1][:]...)
	l = append(l, bls[2][:]...)
	l = append(l, bls[3][:]...)
	l = append(l, []byte{1, 1, 1}...)

	bl, _ := p.splitBitmapBitList(l)
	if len(bl) != len(bls)+1 {
		t.Errorf("invalid lenght: expect %d, actual %d", len(bls)+1, len(bl))
	}

	for i, b := range bls {
		bi, _ := p.makeBitmapLine(b)

		if bl[i] != bi {
			t.Errorf("invalid value: expect %b, actual %b", bi, bl[i])
		}
	}
	bi, _ := p.makeBitmapLine([8]byte{1, 1, 1, 0, 0, 0, 0, 0})
	if bl[len(bl)-1] != bi {
		t.Errorf("invalid value: expect %b, actual %b", bi, bl[len(bl)-1])
	}

}

func TestConvertBitmapToByteList(t *testing.T) {
	p := &DpEh600{}

	ll, err := p.convertBitmapToByteList([][]byte{{0, 1, 1, 0}, {1, 0, 0, 1}, {1, 0, 0, 1}, {1, 1, 1, 1}})
	if err != nil {
		t.Error(err)
	}

	b, _ := p.makeBitmapLine([8]byte{0, 1, 1, 0, 0, 0, 0, 0})
	if ll[0] != b {
		t.Errorf("invalid value: expect %b, actual %b", b, ll[0])
	}
}
