package nausea

import (
	"github.com/jacobsa/go-serial/serial"
	"io"
	"log"
)

type (
	DpEh600 struct {
		port io.ReadWriteCloser
	}
)

func NewDpEh600(opt serial.OpenOptions) (*DpEh600, error) {
	port, err := serial.Open(opt)
	if err != nil {
		log.Fatalf("serial.Open: %v", err)
	}

	return &DpEh600{
		port: port,
	}, nil
}

func (p *DpEh600) Write(b []byte) error { // TODO
	_, err := p.port.Write(b)
	if err != nil {
		return err
	}

	return nil
}

func (p *DpEh600) Close() { // TODO
	p.port.Close()
}

func (p *DpEh600) SetPrintParameter(point byte, time byte, interval byte) {
	cmd := []byte{27}
	cmd = append(cmd, []byte("7")...)
	cmd = append(cmd, point, time, interval)
	p.Write(cmd)
}

func (p *DpEh600) TurnStatusUpload(allowUpload bool, allowLackOfControl bool) {
	var v byte = 0

	if allowUpload {
		v |= 1 << 5
	}
	if allowLackOfControl {
		v |= 1 << 2
	}

	cmd := []byte{29} // GS
	cmd = append(cmd, []byte("a")...)
	cmd = append(cmd, v)

	p.Write(cmd)
}

func (p *DpEh600) PrintBitmap(bitmap [][]byte) {
	h := len(bitmap)
	yl := byte(h % 256)
	yh := byte((h - int(yl)) / 256)

	l, _ := p.convertBitmapToByteList(bitmap) // TODO
	var bl []byte
	for _, n := range l {
		bl = append(bl, byte(n))
	}

	cmd := []byte{29} // GS
	cmd = append(cmd, []byte("v0")...)
	cmd = append(cmd, []byte{0, byte(len(bl)), 0, yl, yh}...)
	cmd = append(cmd, bl...)
	p.Write(cmd)
}

func (p *DpEh600) makeBitmapLine(l [8]byte) (int64, error) {
	var b int64

	for i, v := range l {
		b |= int64(v << (7 - i))
	}

	return b, nil
}

func (p *DpEh600) splitBitmapBitList(l []byte) ([]int64, error) {
	const siz = 8
	var bl []int64

	for i := 0; i < len(l); i += siz {
		var tbl []byte

		if len(l) < siz {
			tbl = l[:]
		} else {
			tbl = l[i : i+siz]
		}

		for {
			if len(tbl) == siz {
				break
			}
			tbl = append(tbl, 0)
		}

		var ttbl [8]byte
		for j, n := range tbl {
			ttbl[j] = n
		}

		b, err := p.makeBitmapLine(ttbl)
		if err != nil {
			return nil, err
		}

		bl = append(bl, b)
	}

	return bl, nil

}

func (p *DpEh600) convertBitmapToByteList(bitmap [][]byte) ([]int64, error) {
	var b []int64
	for _, l := range bitmap {
		nl, err := p.splitBitmapBitList(l)
		if err != nil {
			return nil, err
		}

		b = append(b, nl...)
	}

	return b, nil
}
