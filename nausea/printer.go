package nausea

type (
	Printer interface {
		Write(b []byte) error        // TODO
		PrintBitmap(bitmap [][]byte) // TODO
		Close()                      // TODO
	}
)
