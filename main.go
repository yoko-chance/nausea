package main

import (
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/jacobsa/go-serial/serial"
	"gitlab.com/yoko-chance/nausea/nausea"
	"gitlab.com/yoko-chance/slurp"
	"golang.org/x/term"
)

type (
	Listener func(l string) (string, error)
)

func main() {
	if term.IsTerminal(0) {
		return
	}

	p, err := nausea.NewDpEh600(serial.OpenOptions{
		PortName:              "/dev/serial0",
		BaudRate:              9600,
		DataBits:              8,
		StopBits:              1,
		RTSCTSFlowControl:     true,
		InterCharacterTimeout: 100,
		//MinimumReadSize:   4,
	})
	defer p.Close()
	if err != nil {
		log.Fatal(err)
	}

	p.TurnStatusUpload(true, true)
	p.SetPrintParameter(5, 100, 10)

	slurp.ReadLines(os.Stdin, makeBitmapPrintHandler(p))
}

func makeTextPrintHandler(p nausea.Printer) slurp.Handler {
	return func(l string) error {
		p.Write([]byte(l + "\n"))

		return nil
	}
}

func makeBitmapPrintHandler(p nausea.Printer) slurp.Handler {
	return func(l string) error {
		sl := strings.Split(l, "")

		var b []byte
		for _, s := range sl {
			u, err := strconv.ParseUint(s, 2, 8)
			if err != nil {
				return err
			}

			b = append(b, byte(u))
		}

		p.PrintBitmap([][]byte{b})

		return nil
	}
}
