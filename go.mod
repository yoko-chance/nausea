module gitlab.com/yoko-chance/nausea

go 1.22.1

require (
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4
	gitlab.com/yoko-chance/slurp v0.2.3
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf
)

require golang.org/x/sys v0.0.0-20201214210602-f9fddec55a1e // indirect
